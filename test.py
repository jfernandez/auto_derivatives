# -*- coding: utf-8 -*-
import numpy as np
from math import sqrt
import auto_derivatives as ad


def pow32(x):
    t = sqrt(x)
    return t*t*t

def point_point_generated_gradhess(a0, a1, a2, b0, b1, b2):
    grad = np.zeros(6).tolist()
    hess = np.zeros((6, 6)).tolist()
    
    a = [a0, a1, a2]
    b = [b0, b1, b2]
    
    x0 = a[0] - b[0];
    x1 = a[1] - b[1];
    x2 = a[2] - b[2];
    x3 = (x0 * x0) + (x1 * x1) + (x2 * x2);
    x4 = 1.0/sqrt(x3);
    grad[5] = x4*(-a[2] + b[2]);
    grad[4] = x4*(-a[1] + b[1]);
    grad[3] = x4*(-a[0] + b[0]);
    grad[2] = x2*x4;
    grad[1] = x1*x4;
    grad[0] = x0*x4;
    x5 = 1.0/x3;
    hess[0][0] = x4*(1 - (x0 * x0)*x5);
    hess[3][3] = hess[0][0];
    x3 = 1.0/pow32(x3);
    hess[0][4] = x0*x3*x1;
    hess[4][0] = hess[0][4];
    hess[3][1] = hess[0][4];
    hess[1][3] = hess[0][4];
    hess[0][1] = -hess[0][4];
    hess[4][3] = hess[0][1];
    hess[3][4] = hess[0][1];
    hess[1][0] = hess[0][1];
    hess[0][5] = x0*x3*x2;
    hess[5][0] = hess[0][5];
    hess[3][2] = hess[0][5];
    hess[2][3] = hess[0][5];
    hess[0][2] = -hess[0][5];
    hess[5][3] = hess[0][2];
    hess[3][5] = hess[0][2];
    hess[2][0] = hess[0][2];
    hess[0][3] = x4*((x0 * x0)*x5 - 1);
    hess[3][0] = hess[0][3];
    hess[1][1] = x4*(1 - (x1 * x1)*x5);
    hess[4][4] = hess[1][1];
    hess[1][5] = x3*x1*x2;
    hess[5][1] = hess[1][5];
    hess[4][2] = hess[1][5];
    hess[2][4] = hess[1][5];
    hess[1][2] = -hess[1][5];
    hess[5][4] = hess[1][2];
    hess[4][5] = hess[1][2];
    hess[2][1] = hess[1][2];
    hess[1][4] = x4*((x1 * x1)*x5 - 1);
    hess[4][1] = hess[1][4];
    hess[2][2] = x4*(1 - (x2 * x2)*x5);
    hess[5][5] = hess[2][2];
    hess[2][5] = x4*((x2 * x2)*x5 - 1);
    hess[5][2] = hess[2][5];
        
    return grad, hess


# Point-Point distance
a = ad.symbolic_vector("a", 3)
b = ad.symbolic_vector("b", 3)
variables = ad.get_variables([a, b])

# Function
vab = a - b
d = vab.norm()

# Derivatives
grad = ad.gradient(d, variables)
hess = ad.hessian(d, variables)

ad.test_with_finite_differences(d, grad, hess, point_point_generated_gradhess, 
                             np.random.rand(6), variables)


