# -*- coding: utf-8 -*-
"""
Gradient and Hessian analytic computation and C code generation using Sympy.

This code is not meant to be a complete library to automate complex symbolic
expressions. Instead, it is a collection of functions that I wrote to generate
C code that computes hessians of common collision constraints in physically
based animation contexts.

@author: jfernandez
"""

import copy
import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt
import sympy
from sympy.printing import ccode
import re
sympy.init_printing(pretty_print=False)

"""
Auxiliar functions
"""
def is_iterable(v):
    iterable = True
    try:
        _ = (e for e in v)
    except TypeError:
       iterable = False
    return iterable
    
def tokenize_tmp_variables(txt):
    out = []
    
    i = 0
    while i < len(txt):
        out.append(txt[i])
        i += 1
        
        if txt[i-1] == 'x':
            while i < len(txt) and txt[i].isnumeric():
                out.append(txt[i])
                i += 1
            out.append('|')
    
    return ''.join(out)


"""
Utility functions:
"""
def symbolic_vector(name, size, index_operator="[]"):
    """
    Creates a symbolic vector.

    Parameters
    ----------
    name : str
        Name of the vector.
    size : int
        Size of the vector.
    index_operator : str, optional
        "[]" or "()". The default is "[]".

    Returns
    -------
    sympy.Matrix
        Vector as sympy.Matrix with one column.
    """
    elems = []
    for row in range(size):
        if index_operator == "[]":
            elems.append( [sympy.Symbol("%s[%d]" % (name, row), real=True)] )
        elif index_operator == "()":
            elems.append( [sympy.Symbol("%s(%d)" % (name, row), real=True)] )
        else:
            raise ValueError("Error: index_operator must be '[]' or '()'")
    return sympy.Matrix(elems)

def symbolic_matrix(name, shape, is_symmetric=False, index_operator="(,)"):
    """
    Creates a symbolic vector.

    Parameters
    ----------
    name : str
        Name of the vector.
    size : int
        Size of the vector.
    is_symmetric : bool, optional
        If True, upper triangular is assigned to lower triangular. The default 
        is False.
    index_operator : str, optional
        '[][]', '(,)' or '[,]'. The default is "(,)".

    Returns
    -------
    sympy.Matrix
        Vector as sympy.Matrix with one column.
    """
    out = []
    for i in range(shape[0]):
        out.append([])
        for j in range(shape[1]):
            if index_operator == "[][]":
                out[-1].append(sympy.Symbol('%s[%d][%d]' % (name, i, j), real=True))
            elif index_operator == "(,)":
                out[-1].append(sympy.Symbol('%s(%d, %d)' % (name, i, j), real=True))
            elif index_operator == "[,]":
                out[-1].append(sympy.Symbol('%s[%d, %d]' % (name, i, j), real=True))
            else:
                raise ValueError("Error: index_operator must be '[][]', '(,)' or '[,]'")
    m = sympy.Matrix(out)
    
    if is_symmetric:
        for i in range(shape[0]):
            for j in range(i + 1, shape[1]):
                m[i, j] = m[j, i]
    
    return m

def get_variables(lst):
    """
    Returns all the variables in a sequence of iterables, for example, a 
    sequence of vectors.

    Parameters
    ----------
    lst : list
        List of sympy iterables.

    Returns
    -------
    var : list
        Sympy variables.

    """
    var = []
    for item in lst:
        if type(item) == sympy.Symbol:
            var.append(item)
        else:
            for v in item:
                var.append(v)
    return var

def gradient(exp, variables):
    """
    Computes the gradient of an expression wrt to a set of variables.

    Parameters
    ----------
    exp : sympy expression
        Expression to take derivatives from.
    variables : list
        List of sympy variables contained in the exp.

    Returns
    -------
    sympy.Matrix
        Gradient.

    """
    g = []
    if is_iterable(exp):
        for outer in exp:
            for v in variables:
                g.append(sympy.diff(outer, v))
    else:
        for v in variables:
            g.append(sympy.diff(exp, v))
    return sympy.Matrix(g).transpose()

def hessian(exp, variables, is_symmetric=True):
    """
    Computes the hessian of an expression wrt to a set of variables.

    Parameters
    ----------
    exp : sympy expression
        Expression to take derivatives from.
    variables : list
        List of sympy variables contained in the exp.
    is_symmetric : bool
        If true, copies the lower triangular part into the upper triangular.
        (default: True)

    Returns
    -------
    sympy.Matrix
        Hessian.
    """
    n = len(variables)
    hess = sympy.eye(n)
    if is_iterable(exp):
        raise ValueError("Expression is iterable in hessian()")
        # for v_row in variables:
        #     for outer in exp:
        #         h.append([])
        #         first_derivative_exp = sympy.diff(outer, v_row)
        #         for v_col in variables:
        #             h[-1].append(sympy.diff(first_derivative_exp, v_col))
    else:
        if is_symmetric:
            for i in range(n):
                first_derivative_exp = sympy.diff(exp, variables[i])
                for j in range(0, i+1):
                    hess[i, j] = sympy.diff(first_derivative_exp, variables[j])
                    if i != j:
                        hess[j, i] = hess[i, j]
        else:
            for i in range(n):
                first_derivative_exp = sympy.diff(exp, variables[i])
                for j in range(n):
                    hess[i, j] = sympy.diff(first_derivative_exp, variables[j])
            
    return hess


def substitute_C_operations(txt):
    """
    Replace unconvenient (1.0/2.0, pow(a, 2), ...) C code expressions with 
    better ones (0.5, a*a).

    Parameters
    ----------
    txt : str
        C code with unconvenient expressions.

    Returns
    -------
    aux : str
        Replaced code.
    """
    # Replace inverse powers
    aux = re.sub(r"pow\((.*?), -(.*?)\)", r"(1.0/pow(\1, \2))", txt)
    
    # Standard powers
    aux = re.sub(r"pow\((.*?), 2\)", r"((\1) * (\1))", aux)
    aux = re.sub(r"pow\((.*?), 3\)", r"((\1) * (\1) * (\1))", aux)
    aux = re.sub(r"pow\((.*?), 4\)", r"((\1) * (\1) * (\1) * (\1))", aux)
    aux = re.sub(r"pow\((.*?), 5\)", r"((\1) * (\1) * (\1) * (\1) * (\1))", aux)
    
    # Sqrt
    aux = re.sub(r"pow\((.*?), 1.0/2.0\)", r"sqrt(\1)", aux)
    
    # pow(x, 3/2)
    aux = re.sub(r"pow\((.*?), 3.0/2.0\)", r"pow32(\1)", aux)
    
    # Others
    aux = aux.replace("1.0/2.0", "0.5")
    
    return aux

def print_symbolic(matrix):
    """
    Prints ASCII sympy matrices, vectors or single expressions.
    """
    if type(matrix) != sympy.Matrix:
        print(matrix)
    else:
        for i in range(matrix.shape[0]):
            for j in range(matrix.shape[1]):
                print("\n\n[%d, %d]\n%s" % (i, j, matrix[i, j]))


"""
Core functions:
"""
def multi_expression_to_C_code_raw(exp_lst, names, vector_index_operator="[]", 
                               matrix_index_operator="(,)",
                               substitute_C_operators=True):
    """
    Generates C code that computes a sequence of expressions with the minimum
    amount of operations possible. This function does not postprocess the 
    generated code for higher efficiency.

    Parameters
    ----------
    exp_lst : list
        Sympy expression list.
    names : list
        Variable name of each expression.
    vector_index_operator : str, optional
        "[]" or "()". The default is "[]".
    matrix_index_operator : str, optional
        '[][]', '(,)' or '[,]'. The default is "(,)".
    substitute_C_operators : bool (default: True)
        Substitute functions like pow to more efficient C variants.

    Returns
    -------
    tmp : list
        Temporal calculations.
    out : list
        Output expressions.
    op_count : list
        Number of operations per expression.

    """
    tmp, out, op_count = [], [], []
    cse = sympy.cse(exp_lst)
    for e in cse[0]:
        tmp.append('%s = %s' % (str(e[0]), ccode(e[1])))
        op_count.append(e[1].count_ops())
    
    for name, matrix in zip(names, cse[1]):
        
        # Single expression
        if type(matrix) != sympy.Matrix:
            out.append('%s = %s' % (name, ccode(matrix)))
            
        # Matrix expression
        else:
        
            # Vector
            if 1 in matrix.shape:
                for i, v in enumerate(matrix):
                    if vector_index_operator == "[]":
                        out.append('%s[%d] = %s' % (name, i, ccode(v)))
                    elif vector_index_operator == "()":
                        out.append('%s(%d) = %s' % (name, i, ccode(v)))
                    else:
                        raise ValueError("Error: vector_index_operator must be '[]' or '()'")
    
            # Matrix
            else:
                for i in range(matrix.shape[0]):
                    for j in range(matrix.shape[1]):
                        if matrix_index_operator == "[][]":
                            out.append('%s[%d][%d] = %s' % (name, i, j, ccode(matrix[i, j])))
                        elif matrix_index_operator == "(,)":
                            out.append('%s(%d, %d) = %s' % (name, i, j, ccode(matrix[i, j])))
                        elif matrix_index_operator == "[,]":
                            out.append('%s[%d, %d] = %s' % (name, i, j, ccode(matrix[i, j])))
                        else:
                            raise ValueError("Error: matrix_index_operator must be '[][]', '(,)' or '[,]'")
    
    # Replace unconvenient C expressions
    if substitute_C_operators:
        tmp = substitute_C_operations('\n'.join(tmp)).split('\n')
        out = substitute_C_operations('\n'.join(out)).split('\n')
            
    return tmp, out, op_count


def multi_expression_to_C_code(exp_lst, names, real_type='double', min_tmp_op=0,
                            vector_index_operator="[]", matrix_index_operator="(,)",
                            substitute_C_operators=True):
    """
    Generates C code that computes a sequence of expressions with the minimum
    amount of operations possible.


    Parameters
    ----------
    exp_lst : list
        Sympy expression list.
    names : list
        Variable name of each expression.
    real_type : str, optional
        Scalar or real type. The default is 'Real'.
    min_tmp_op : int, optional
        Removes tmp expressions with a single operation if they are used less 
        than min_tmp_op times. Helps reducing the number of tmp variables used.
        The default is 3.
    vector_index_operator : str, optional
        "[]" or "()". The default is "[]".
    matrix_index_operator : str, optional
        '[][]', '(,)' or '[,]'. The default is "(,)".
    substitute_C_operators : bool (default: True)
        Substitute functions like pow to more efficient C variants.


    Returns
    -------
    str
        Resulting C code.

    """
    # Input check
    for name in names:
        if name.startswith('x'):
            print('Error: Variables starting with x are reserved.')
            return
        
    # Use sympy to reduce the common expressions
    tmp, out, op_count = multi_expression_to_C_code_raw(exp_lst, names,
                                vector_index_operator, matrix_index_operator,
                                substitute_C_operators)
    n_tmp = len(tmp)
    
    if tmp[0] == '':
        return "\n".join(out)
    
    # Tokenize. Add '.' at the end of each tmp variable
    tmp_txt = tokenize_tmp_variables('\n'.join(tmp))
    out_txt = tokenize_tmp_variables('\n'.join(out))
    tmp = tmp_txt.split('\n')
    out = out_txt.split('\n')
    
    # ERROR: This is wrong!
    # # Remove tmp variables that are used less than certain amount of times
    # all_txt = tmp_txt + '\n' + out_txt
    # for line_i in range(len(tmp)):
    #     if op_count[line_i] == 1:
    #         var_i = tmp[line_i].split(' = ')[0]
    #         times_used = all_txt.count(var_i) - 1
            
    #         if times_used < min_tmp_op:
    #             exp_i = tmp[line_i].split(' = ')[1]
                
    #             for line_j in range(len(tmp)):
    #                 tmp[line_j] = tmp[line_j].replace(var_i, "(%s)" % exp_i)
    #             for line_j in range(len(out)):
    #                 out[line_j] = out[line_j].replace(var_i, "(%s)" % exp_i)
                
    #             tmp[line_i] = ''
    # tmp = [l for l in tmp if l != '']
    # tmp_txt = '\n'.join(tmp)
    # out_txt = '\n'.join(out)
        
    # Remove temp variable that are equal to an output entry
    tmp_map = {e.split(' = ')[0]: e.split(' = ')[1] for e in tmp}
    for out_entry in out:
        v = out_entry.split(' = ')[1]
        if v in tmp_map:
            tmp_txt = tmp_txt.replace(v, out_entry.split(' = ')[0])
            out_txt = out_txt.replace(v, out_entry.split(' = ')[0])
    tmp = tmp_txt.split('\n')
    out = out_txt.split('\n')
    
    # Remove redundant assignments from the output
    out_non_redundant = []
    for out_line in out:
        if out_line.split(' = ')[0] != out_line.split(' = ')[1]:
            out_non_redundant.append(out_line)
    out = out_non_redundant
    out_txt = '\n'.join(out)
    
    # Move up the output entries to the line they can be computed
    for out_line in out:
        last_line_relevant = -1
        for line_i, line in enumerate(tmp):
            var_i = tmp[line_i].split(' = ')[0]
            if var_i in out_line:
                last_line_relevant = line_i
        
        new_tmp = tmp[:last_line_relevant + 1]
        new_tmp.append(out_line)
        new_tmp += tmp[last_line_relevant + 1:]
        tmp = copy.copy(new_tmp)
    
    # Reuse tmp variables
    ## At each line, look for unused tmp variables
    ## Repeat this operation until tmp variable i reaches output or the last tmp variable
    for line_i in range(len(tmp) - 1):
        if tmp[line_i].startswith('x'):
            var_i = tmp[line_i].split(' = ')[0]
            
            # Look for an unused tmp var
            found = False
            for line_j in range(line_i):
                if tmp[line_j].startswith('x'):
                    var_j = tmp[line_j].split(' = ')[0]
                    
                    remaining_tmp_txt = ''.join(tmp[line_i+1:])
                    #if (var_j not in out_txt) and (var_j not in remaining_tmp_txt):
                    if var_j not in remaining_tmp_txt:
                        found = True
                        break
                
            if found:
                for i in range(line_i, len(tmp)):
                    tmp[i] = tmp[i].replace(var_i, var_j)

    # Rename remaining tmp variables from 0 to n without gaps (remove tokenization)
    tmp_txt = '\n'.join(tmp)
    next_tmp = 0
    for tmp_i in range(n_tmp):
        var_i = 'x%d|' % tmp_i
        if var_i in tmp_txt:
            var_j = 'tmp[%d]' % next_tmp
            tmp_txt = tmp_txt.replace(var_i, var_j)
            next_tmp += 1
            
    # # Declaration
    declaration_txt = real_type + ' tmp[%d];' % next_tmp
    
    # Add semicolons
    tmp_txt = '\n'.join([l + ';' for l in tmp_txt.split('\n')])
    
    # Replace strings of the kind: + (-x) not followed by * or /
    tmp_txt = re.sub(r"\+\s*\((-.*?)\)(?!\s*[*/])", r"\1", tmp_txt)
    
    return declaration_txt + '\n\n' + tmp_txt


def test_with_finite_differences(v, grad, hess, generated_f, x, variables):
    """
    Test the gradient and hessian from sympy and the generated code.

    Parameters
    ----------
    v : sympy expression
        Function value.
    grad : sympy vector
        Gradient of v.
    hess : sympy matrix
        Hessian of v.
    generated_f : function
        Python function that runs the generated code to compute the gradient 
        and hessian of v.
    x : list
        Point to be evaluated at.
    var : list
        Sympy variables that grad and hess have been derived wrt.

    Returns
    -------
    None.

    """
    EPS = np.sqrt(1e-16)
    x = [float(v) for v in x]
    
    f = sympy.lambdify(variables, v)
    f_grad = sympy.lambdify(variables, grad)
    f_hess = sympy.lambdify(variables, hess)

    # Sympy
    gx = f_grad(*x)
    hx = f_hess(*x)
    
    # Generated code
    g_cse, h_cse = generated_f(*x)

    # Finite differences
    # Gradient
    ndofs = len(x)
    gx_fd = np.zeros(ndofs)
    for i in range(ndofs):
        x[i] += EPS
        f_plus = f(*x)
        x[i] -= 2*EPS
        f_minus = f(*x)
        x[i] += EPS
        
        gx_fd[i] = (f_plus - f_minus) / (2*EPS)
    
    print('Grad error norm finite differences: %.4e' % np.linalg.norm(np.array(gx) - np.array(gx_fd)))
    print('Grad error norm generated code: %.4e' % np.linalg.norm(np.array(gx) - np.array(g_cse)))
    
    # Hessian (https://v8doc.sas.com/sashtml/iml/chap11/sect8.htm)
    hx_fd = np.zeros([ndofs, ndofs])
    for i in range(ndofs):
        j = i
        x[i] += EPS
        g_plus_i = f_grad(*x)
        x[i] -= 2*EPS
        g_minus_i = f_grad(*x)
        x[i] += EPS
        
        x[j] += EPS
        g_plus_j = f_grad(*x)
        x[j] -= 2*EPS
        g_minus_j = f_grad(*x)
        x[j] += EPS
        
        hx_fd[i] = (g_plus_i - g_minus_i) / (4*EPS) + (g_plus_j - g_minus_j) / (4*EPS)
    
    print('Hess error norm finite differences: %.4e' % np.linalg.norm(np.array(hx) - np.array(hx_fd)))
    print('Hess error norm generated code: %.4e' % np.linalg.norm(np.array(hx) - np.array(h_cse)))


def is_symmetric(A, rtol=1e-05, atol=1e-08):
    dofs = A.free_symbols
    f = sympy.lambdify(dofs, A)
    for _ in range(100):
        A_eval = f(*np.random.rand(len(dofs))*0.5)
        if not np.allclose(A_eval, A_eval.T, rtol=rtol, atol=atol):
            return False
    return True
    
    
def plot_symbolic_expression(exp, variables, constants, plot_range=0.5, 
                             resolution=1000, ax=None):
    """
    Plots a scalar multivariate symbolic expression. Additionally, it returns
    a matrix containing the residuals from polynomial fits to each variable.
    
    
    Parameters
    ----------
    exp : sympy expression
        Multivariate symbolic expression to be plotted.
    variables : list of sympy symbols
        Variables in the expression to plot along.
    constants : list of sympy symbols
        Constants in the expression.
    plot_range : float
        The variables will be plotted in the range 
        [min - plot_range, min + plot_range], where min is the expression 
        minimum.
    resolution : int
        Number of sample points for the plots.
    ax : matplotlib.axes
        If None, a new figure will be created. Otherwise, the plots will be 
        laid on the passed axes.

    Returns
    -------
    ax : matplotlib.axes
        Axes with the results.
    polynomial_residual : numpy.array
        Matrix containing the residuals from polynomial fits to each variable.
        Rows are the variables, columns are the degrees of the polynomial fit.
        
    TODO:
    	Automatically finding the minimum is not always a good idea. For
    	example, the minimum distance for Triangle-Point is that the
    	4 points overlap. This breaks all the remaining steps. We need
    	to be able control the variables "center" and their individual
    	range. This way, we can set up a proper triangle and point
    	configurations and give reasonable deltas for possible motions.
    """
    if ax == None:
        fig = plt.figure()
        ax = fig.add_subplot()
    
    # Random constants
    constants_subs = dict(zip(constants, np.random.rand(len(constants))))
    exp_ = exp.subs(constants_subs)
    
    # Make numeric function
    ## Creates a python function from sympy expression
    f_individual_args = sympy.lambdify(variables, exp_) 
    
    ## Wrap the function so it takes a list as arguments instead of individual ones
    f = lambda x: f_individual_args(*x)
    
    # Find the minimum of the function (center of the plot)
    x = np.random.rand(len(variables))
    x_zero = scipy.optimize.minimize(f, x).x
    
    ## Plot +/- range for each variable
    plot_range_delta = np.linspace(-plot_range, plot_range, resolution)
    polynomial_residual = []
    for var_i in range(len(variables)):
        x_axis = plot_range_delta + x_zero[var_i]
        x_eval = x_zero.copy()
        y_axis = np.zeros(len(x_axis))
        for i, x_value in enumerate(x_axis):
            x_eval[var_i] = x_value
            y_axis[i] = f(x_eval)
        ax.plot(plot_range_delta, y_axis, label=str(var_i))
            
        # Fit to polynomials
        residual_var_i = []
        for degree in range(1, 5):
            fit_results = np.polyfit(plot_range_delta, y_axis, degree, full=True)
            residual_var_i.append(fit_results[1][0])
        polynomial_residual.append(residual_var_i)
    
    return ax, np.array(polynomial_residual)
    
    
    
    
    
    
    
    
    