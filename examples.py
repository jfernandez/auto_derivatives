# -*- coding: utf-8 -*-
import auto_derivatives as ad

generated_code = []

# Point-Point distance
if True:
    a = ad.symbolic_vector("a", 3)
    b = ad.symbolic_vector("b", 3)
    variables = ad.get_variables([a, b])
    
    # Function
    vab = a - b
    d = vab.norm()
    
    # Derivatives
    grad = ad.gradient(d, variables)
    hess = ad.hessian(d, variables)
    txt = ad.multi_expression_to_C_code([grad, hess], ["grad", "hess"])
    generated_code.append(txt)  # See this generated code at the end
    
# Point-Triangle distance
if True:
    a = ad.symbolic_vector("a", 3)
    b = ad.symbolic_vector("b", 3)
    c = ad.symbolic_vector("c", 3)
    p = ad.symbolic_vector("p", 3)
    variables = ad.get_variables([a, b, c, p])
    
    # Function
    vpa = p - a
    vac = a - c
    vbc = b - c
    n = vac.cross(vbc)
    n_normalized = n / n.norm()
    d = vpa.dot(n_normalized)
    
    # Derivatives
    grad = ad.gradient(d, variables)
    hess = ad.hessian(d, variables)
    txt = ad.multi_expression_to_C_code([grad, hess], ["grad", "hess"])
    generated_code.append(txt)

# Line-Line distance
if True:
    a = ad.symbolic_vector("a", 3)
    b = ad.symbolic_vector("b", 3)
    p = ad.symbolic_vector("p", 3)
    q = ad.symbolic_vector("q", 3)
    variables = ad.get_variables([a, b, p, q])
    
    # Function
    vba = b - a
    vqp = q - p
    n = vba.cross(vqp)
    d = n.dot(a - p) / n.norm()
    
    # Derivatives
    grad = ad.gradient(d, variables)
    hess = ad.hessian(d, variables)
    txt = ad.multi_expression_to_C_code([grad, hess], ["grad", "hess"])
    generated_code.append(txt)



"""
Result of Point-Point distance:

    double tmp[9];
    
    tmp[0] = a[0] - b[0];
    tmp[1] = a[1] - b[1];
    tmp[2] = a[2] - b[2];
    tmp[3] = ((tmp[0] * tmp[0])) + ((tmp[1] * tmp[1])) + ((tmp[2] * tmp[2]));
    dist = sqrt(tmp[3]);
    tmp[4] = 1.0/dist;
    grad[2] = tmp[2]*tmp[4];
    grad[1] = tmp[1]*tmp[4];
    grad[0] = tmp[0]*tmp[4];
    tmp[5] = -a[0] + b[0];
    grad[3] = tmp[4]*tmp[5];
    tmp[6] = -a[1] + b[1];
    grad[4] = tmp[6]*tmp[4];
    tmp[7] = -a[2] + b[2];
    grad[5] = tmp[7]*tmp[4];
    tmp[3] = 1.0/pow32(tmp[3]);
    hess(5, 0) = (tmp[7]*(tmp[3]*tmp[5]));
    hess(4, 0) = (tmp[6]*(tmp[3]*tmp[5]));
    hess(3, 2) = (tmp[7]*(tmp[3]*tmp[5]));
    hess(3, 1) = (tmp[6]*(tmp[3]*tmp[5]));
    tmp[8] = tmp[0]*tmp[3];
    hess(5, 3) = (tmp[7]*tmp[8]);
    hess(4, 3) = (tmp[6]*tmp[8]);
    hess(2, 3) = (tmp[8]*tmp[2]);
    hess(1, 3) = (tmp[8]*tmp[1]);
    hess(0, 5) = (tmp[8]*tmp[2]);
    hess(0, 4) = (tmp[8]*tmp[1]);
    hess(0, 2) = (tmp[7]*tmp[8]);
    hess(0, 1) = (tmp[6]*tmp[8]);
    hess(0, 0) = tmp[8]*tmp[5] + tmp[4];
    hess(3, 3) = hess(0, 0);
    tmp[8] = -tmp[4];
    hess(5, 2) = (tmp[7] * tmp[7])*tmp[3] + tmp[8];
    hess(4, 1) = (tmp[6] * tmp[6])*tmp[3] + tmp[8];
    hess(3, 0) = tmp[3]*(tmp[5] * tmp[5]) + tmp[8];
    hess(2, 5) = tmp[3]*((tmp[2] * tmp[2])) + tmp[8];
    hess(1, 4) = tmp[3]*((tmp[1] * tmp[1])) + tmp[8];
    hess(0, 3) = ((tmp[0] * tmp[0]))*tmp[3] + tmp[8];
    tmp[0] = tmp[3]*tmp[1];
    hess(5, 4) = (tmp[7]*tmp[0]);
    hess(3, 4) = (tmp[0]*tmp[5]);
    hess(2, 4) = (tmp[0]*tmp[2]);
    hess(1, 5) = (tmp[0]*tmp[2]);
    hess(1, 2) = (tmp[7]*tmp[0]);
    hess(1, 0) = (tmp[0]*tmp[5]);
    hess(1, 1) = tmp[6]*tmp[0] + tmp[4];
    hess(4, 4) = hess(1, 1);
    tmp[0] = tmp[3]*tmp[2];
    hess(4, 5) = (tmp[6]*tmp[0]);
    hess(3, 5) = (tmp[0]*tmp[5]);
    hess(2, 1) = (tmp[6]*tmp[0]);
    hess(2, 0) = (tmp[0]*tmp[5]);
    hess(2, 2) = tmp[7]*tmp[0] + tmp[4];
    hess(5, 5) = hess(2, 2);
    hess(4, 2) = tmp[6]*tmp[7]*tmp[3];
    hess(5, 1) = hess(4, 2);
"""